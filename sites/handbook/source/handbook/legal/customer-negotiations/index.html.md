---
layout: handbook-page-toc
title: "Sales Guide: Collaborating with GitLab Legal"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}. 
## OVERVIEW 
Thank you for visiting! The purpose of this resource is to provide Sales reps assistance on: 
    
(1) Operational - How to work with, and engage, GitLab legal; Requirements to close a deal; General FAQ for legal ops, including some parellels to Finance and Deal Desk matters. 

(2) Educational - Learning about how and why GitLab legal and our agreements work, to better serve you during a sales cycle; Proactive advice and education to enable you to feel more confident when / if your Customer has legal related questions.   

## ENGAGEMENT
- As part of this collaboration, we are measuring success by interaction with the field. For this reason, we ask that you please watch the videos and "thumbs-up" or "like" them.

## REQUESTING CONTENT
- You are always encouraged to make requests for future content that will help you and the team. Simply complete the form here: https://forms.gle/2zznmLFznSqJAQUH6
  

| OPERATIONAL| EDUCATIONAL |
| ------ | ------ |
| **How to reach Legal**                 | **Overview of GitLab Agreements** |
| **How do I get an Agreement signed**      | **When does GitLab negotiate** |
| COMING SOON: Vendor Request Forms (W9/Insurance)     | COMING SOON:Introduction to Open Source |
| COMING SOON: How Partner's can execute a Partner Agreement      | COMING SOON: GitLab Master Partner Agreement |



## OPERATIONAL

## How to reach Legal 
1. For all questions related to Partners and/or Customers, including: NDA's, Contract Review / Negotiations, Legal Questions...etc., please open a **Legal Request** in SFDC. 
2. Please note: There is a $25K opportunity minimum threshold to review edits to our terms, and a $100K opportunity minimum threshold to review Customer / Partner Terms. These thresholds do not apply to NDA's. 
3. A presentation on this topic can be found here (https://docs.google.com/presentation/d/1lesWNvPAFd1B3RuCgKsqQlE85ZEwLuE01QpVAKPhQKw/edit#slide=id.g5d6196cc9d_2_0). 

## How do I get an Agreement signed
1. Only certain individuals may execute on behalf of GitLab see Authorization Matrix here: https://about.gitlab.com/handbook/finance/authorization-matrix/#authorization-matrix.
2. All Agreements require a GitLab Legal stamp in order to be executed. This stamp is placed by a Contract Manager when an executable version is reached. 
3. Electronic signature requests can be generated from Sertifi in SFDC, OR, HelloSign.  


## EDUCATIONAL

## Overview of GitLab Agreements
1. GitLab provides our Software pursuant to the GitLab Subscription Agreement, and Professional Services pursuant to GitLab Professional Services Terms;
2. GitLab has a Master Partner Agreement within multiple Exhibits to enable Partners to, (i) Reseller, (ii) Refer, and (iii) Distribute GitLab Products and Services;
3. Other than web-portal purchases, GitLab enters into an Order Form with a Customer which outlines the Products and Services being sold;
4. Any modification(s) to Order Forms, or standard Terms and Conditions, require GitLab Legal Approval. 

## When does GitLab negotiate
1. GitLab will negotiate NDAs with our prospects. You can always send the prospect the GitLab NDA template found at the URL below and ask for them to sign. If they want to use their template, simply open a Legal Request https://about.gitlab.com/handbook/legal/#contract-templates 
2. Change the Contract Status to “Approved to Sign”.
3. Send a chatter message to the Sales account owner to stage for signing.
4. Once signed, the sales account owner will be responsible for uploading the final document, filling out all of the remaining fields in the contract record, and changing the Contract Status to "Active"
