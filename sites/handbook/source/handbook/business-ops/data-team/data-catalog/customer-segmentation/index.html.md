---
layout: handbook-page-toc
title: "Customer Segmentation Analysis"
---

## On this page

{:.no_toc}

- TOC
{:toc}

----

## WIP: Customer Segmentation Analysis

Customer segmentation is the process of dividing our customers into groups based on common characteristics so that we can understand who our customers are and provide them with a great customer experience. There are many characteristics that identify our customers including industry, product category, sales segment, Self-Managed or SaaS, and territory to name a few. The Customer Segmentation Analysis page will provide the information and tools that GitLab team members can use to explore customer data and develop customer insights. 

### Knowledge Assessment & Certification

`Coming Soon`

### Data Classification

`Coming Soon`

### Solution Ownership

- Source System Owner: 
    - Salesforce: `@jbrennan1`
    - Zuora: `@andrew_murray` 
- Source System Subject Matter Expert: 
    - Salesforce: `@jbrennan1` 
    - Zuora: `@andrew_murray` 
- Data Team Subject Matter Expert: `@msendal` `@paul_armstrong` `@jeanpeguero` `@jjstark` `@iweeks`   

### Key Terms

1. [Product Category, Product Tier, Delivery](https://about.gitlab.com/handbook/marketing/product-marketing/tiers/#overview)
1. [Sales Segment](https://about.gitlab.com/handbook/sales/field-operations/gtm-resources/#segmentation)
1. [Account Owner Team](https://about.gitlab.com/handbook/sales/#initial-account-owner---based-on-segment)
1. [Territory](https://about.gitlab.com/handbook/sales/territories/#territories)
1. Industry

### Key Metrics, KPIs, and PIs

1. [ARR](https://about.gitlab.com/handbook/sales/#annual-recurring-revenue-arr)
1. [Customer Count](https://about.gitlab.com/handbook/sales/#customer)

## Self-Service Data Solution

### Self-Service Dashboard Viewer

| Dashboard | Purpose | 
| ----- | ----- | 
 | [Customer Segmentation Analysis](https://app.periscopedata.com/app/gitlab/718514/Customer-Segmentation-Analysis) | cell |

### Self-Service Dashboard Developer

| Data Space | Description | 
| ----- | ----- | 
| cell | cell |

### Self-Service SQL Developer 

#### Key Fields and Business Logic

`Coming Soon`

#### Entity Relationship Diagrams

| Diagram/Entity | Grain | Purpose | Keywords | 
| ----- | ----- | ----- |  ----- | 
| [ARR and Customer Count Analytics ERD](https://app.lucidchart.com/documents/edit/bfd9322f-5132-42e4-8584-8230e6e28b87/0_0) |  |  |  |

#### Reference SQL

`Coming Soon`

## Data Platform Solution

### Data Lineage

[dbt arr_data_mart lineage chart](https://dbt.gitlabdata.com/#!/model/model.gitlab_snowflake.arr_data_mart?g_v=1&g_i=%2Barr_data_mart%2B)

### DBT Solution

`Coming Soon`

## Trusted Data Solution
[Trusted Data Framework](https://about.gitlab.com/handbook/business-ops/data-team/direction/trusted-data/)
### EDM Enterprise Dimensional Model Validations

`Coming Soon`

### RAW Source Data Pipeline validations

`Coming Soon`

### Manual Data Validations

`Coming Soon`
